﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Encrypter
{
    public partial class main : Form
    {
        public main()
        {
            InitializeComponent();
        }

        private void namebox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Title = "Browse  Files";
            if (of.ShowDialog() == DialogResult.OK)
            {
                namebox1.Text = of.FileName;
                namebox1.Enabled = false;
            }
        }
        string file;
        private void button16_Click(object sender, EventArgs e)
        {
            if (namebox1.Text ==  "")
            {
                MessageBox.Show("A file must be selected");
            }
            else
            file = namebox1.Text.ToString();
            string extension = Path.GetExtension(file);
                FileInfo f = new FileInfo(file);
            f.MoveTo(Path.ChangeExtension(file, ".txt"));
            MessageBox.Show("File has been Encrypted");
            button15.Enabled = true;
            button12.Enabled = false;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (namebox1.Text == "")
            {
                MessageBox.Show("A file must be selected");
            }
            else
                file = namebox1.Text.ToString();
            string extension = Path.GetExtension(file);
            FileInfo f = new FileInfo(file);
            f.MoveTo(Path.ChangeExtension(file, ".exe"));
            MessageBox.Show("File has been Decrypted");
        }
    }
}
